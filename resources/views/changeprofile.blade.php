@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('content')

<form id="form-change-profile" role="form" method="POST" action="{{ url('/profile') }}" novalidate class="form-horizontal">
  {{csrf_field()}}
  <div class="col-md-9">             
    <label for="current-profile" class="col-sm-4 control-label">Name</label>
    <div class="col-sm-8">
      <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
      </div>
    </div>
    <label for="email" class="col-sm-4 control-label">Email</label>
    <div class="col-sm-8">
      <div class="form-group">
        <input type="email" class="form-control" id="email" name="email" placeholder="Email@xyz.com">
      </div>
    </div>
    
  </div>
  <div class="form-group">
    <div class="col-sm-offset-5 col-sm-6">
      <button type="submit" class="btn btn-danger">Submit</button>
    </div>
  </div>
</form>
@endsection