@extends('adminlte::page')

@section('title', 'Geo Tracker')

@section('content_header')
    <h1>Geo Tracker</h1>
@stop
@section('content')
    <div class="container">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">App User</div>
                    <div class="panel-body">
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>IMEI</th><th>Device Id</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($appUsers as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                         <td>{{ $item->imei }}</td>
                                         <td>{{ $item->deviceId }}</td>

                                        <td>
                                            <a href="{{ url('/appusers/' . $item->id) }}" title="View client"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/appusers/' . $item->id . '/edit') }}" title="Edit client"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/appusers', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete client',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $appUsers->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                
                </div>
            </div>
        </div>
    </div>
@endsection
