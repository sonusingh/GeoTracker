@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://maps.google.com/maps/api/js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>


    <style type="text/css">

        #mymap {
            border:1px solid red;
            width: 800px;
            height: 500px;
        }

    </style>





 


  <div id="mymap"></div>


  <script type="text/javascript">


    var locations =  {!! $locations !!};
    console.log(locations);


    var mymap = new GMaps({

      el: '#mymap',

      lat: 21.170240,

      lng: 72.831061,

      zoom:6

    });


    $.each( locations, function( index, value ){

          mymap.addMarker({
          lat: value.lat,
          lng: value.lng,
          title: value.name,

          click: function(e) {

            alert('This is '+value.city+' ');

          }

        });

   });


  </script>
@endsection
