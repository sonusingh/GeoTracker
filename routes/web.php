<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use MongoDB\Client as Mongo;
//use DB;
Route::get('mongo', function(Request $request) {
  //  $collection = (new Mongo)->geo_tracker->mycollection;
    //return $collection->find()->toArray();
    $users = DB::collection('mycollection')->get();
   return $users;
});
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('/appusers','AppUserController');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/password','HomeController@password');
Route::post('/password','HomeController@changepassword');

Route::get('/profile','HomeController@profile');
Route::post('/profile','HomeController@changeprofile');
