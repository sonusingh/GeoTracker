<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AppUser extends Eloquent
{
    protected $collection='app_users';
    protected $fillable = [
        'name', 'imei', 'phone','email','password','deviceId',
    ];


    protected $hidden = [
        'password',
    ];

    public function location()
    {
        return $this->hasMany('\App\GeoLocation');
    }

 

}
