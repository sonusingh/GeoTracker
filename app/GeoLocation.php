<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GeoLocation extends Eloquent
{
    protected $collection='geo_locations';
    protected $fillable = [
        'lat', 'lng', 'app_user_id',
    ];
    public function user()
    {
        return $this->belongsTo('\App\AppUser');
    }
}
