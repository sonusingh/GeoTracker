<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppUser;
use App\Geolocation;
use Auth;
use Hash;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations=AppUser::with('location')->get();
        return view('home',compact('locations'));
    }


    
     public function password(){
      return view('change_password');
    }

    public function changepassword(Request $request){
         $user=Auth::guard('web')->user();
         $password=$user->password;
         $current_password=$request->current_password;
        
         if(Hash::check($current_password,$password)){
           $newpassword=$request->password;
           $password_confirmation=$request->password_confirmation;
           if($newpassword==$password_confirmation){
            $user->password=$newpassword;
            $user->save();
           }
         }
         return redirect()->back();
    }

    public function profile(){
      return view('changeprofile');
    }

    public function changeprofile(Request $request){
          $user=Auth::guard('web')->user();
          $user->name=$request->name;
          $user->email=$request->email;
          $user->save();
          return redirect()->back();
    }
}
