<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\GeoLocation;
use Illuminate\Http\Request;

class AppUserApiController extends Controller
{
   
    public function location(Request $request)
    {

        $app_user = AppUser::where('deviceId','=',$request->deviceId)->first();
        if($app_user){
              $id=$app_user->id;
              $geo=new GeoLocation();
              $geo->app_user_id=$id;
              $geo->lat=$request->lat;
              $geo->lng=$request->lng;
              $geo->save();
              return response()->json([
                    'success'=> true,
                    'message'=> 'Added user location'
                ]);
        }
        return response()->json([
                    'success'=> false,
                    'message'=> 'no user exist'
                ]);
      
        
    }

    public function store(Request $request)
    {
        $deviceId=AppUser::where('deviceId','=',$request->deviceId)->first();
        if($deviceId){
            return response()->json([
                    'success'=> false,
                    'message'=> 'Already a registered user'
                ]);

        }
        AppUser::create($request->all());
        return response()->json([
                    'success'=> true,
                    'message'=> 'Added user'
                ]);
    }

  
}
