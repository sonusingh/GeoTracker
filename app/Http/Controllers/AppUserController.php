<?php

namespace App\Http\Controllers;
use App\GeoLocation;
use App\AppUser;
use Illuminate\Http\Request;

class AppUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $perPage = 15;
        $appUsers = AppUser::paginate($perPage);
        return view('app_user.index', compact('appUsers'));
    }

  
    public function show($id)
    {
        $locations=AppUser::with('location')->find($id);
        return view('app_user.show', compact('locations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppUser  $appUser
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $appUser=AppUser::find($id);
         return view('app_user.edit', compact('appUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppUser  $appUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $appUser=AppUser::find($id);
        $appUser->update($request->all());
        return redirect('/appusers')->with('flash_message', 'App User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppUser  $appUser
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppUser::destroy($id);
        return redirect('/appusers')->with('flash_message', 'Appuser deleted!');
    }
}
